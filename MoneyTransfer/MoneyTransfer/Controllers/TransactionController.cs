﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using MoneyTransfer.Models;
using MoneyTransfer.Services;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransactionController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly UserService userService;
        private readonly TransactionService transactionService;
        private readonly IStringLocalizer<TransactionController> _localizer;

        public TransactionController(UserManager<User> userManager, UserService userService, 
            TransactionService transactionService, IStringLocalizer<TransactionController> _localizer)
        {
            _userManager = userManager;
            this.userService = userService;
            this.transactionService = transactionService;
            this._localizer = _localizer;
        }

        public async Task<IActionResult> Index()
        {
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            TransactionViewModel model = new TransactionViewModel
            {
                UserBalance = user.UserBalance
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(TransactionViewModel model)
        {
            if (ModelState.IsValid)
            {
                User userSender = await _userManager.FindByNameAsync(User.Identity.Name);
                User userReceiver = await userService.GetUserByUniqueNumber(model.ReceiverUniqueNumber);
                Transaction transaction = new Transaction
                {
                    UserSenderId = userSender.Id,
                    UserReceiverId = userReceiver.Id,
                    TransferAmount = model.TransferAmount,
                    TransferDateTime = DateTime.Now
                };
                userSender.UserBalance -= model.TransferAmount;
                userReceiver.UserBalance += model.TransferAmount;
                await _userManager.UpdateAsync(userSender);
                await _userManager.UpdateAsync(userReceiver);
                transactionService.AddTransaction(transaction);
                RedirectToAction("Journal", "Transaction");
            }
            return View(model);
        }

        public async Task<IActionResult> Journal(DateTime? dateTimeFrom, DateTime? dateTimeTo)
        {
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            IQueryable<Transaction> transactions = transactionService.GetAllTransactions();
            transactions = transactions.Where(tr => tr.UserSenderId == user.Id);
            transactions = transactions.Where(tr => tr.UserReceiverId == user.Id);
            if (dateTimeFrom != null)
            {
                transactions = transactions.Where(tr => tr.TransferDateTime >= dateTimeFrom);
            }
            if (dateTimeTo != null)
            {
                transactions = transactions.Where(tr => tr.TransferDateTime <= dateTimeTo);
            }
            return View(transactions);
        }

        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> CheckUserBalance(int TransferAmount)
        {
            User userSender = await _userManager.FindByNameAsync(User.Identity.Name);
            return Json(TransferAmount < userSender.UserBalance);
        }

        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> CheckUserReceiverExistsByUniqueNumber(string ReceiverUniqueNumber)
        {
            User userReceiver = await userService.GetUserByUniqueNumber(ReceiverUniqueNumber);
            return Json(userReceiver != null);
        }
    }
}