﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }

        public string UserSenderId { get; set; }
        public User UserSender { get; set; }
        public string UserSenderUniqueNumber { get; set; }

        public string UserReceiverId { get; set; }
        public User UserReceiver { get; set; }
        public string UserReceiverUniqueNumber { get; set; }

        public int TransferAmount { get; set; }

        public DateTime TransferDateTime { get; set; }
    }
}
