﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MoneyTransfer.ViewModels
{
    public class TransactionViewModel
    {
        [Required(ErrorMessage = "UserUniqueNumberErrorMessage")]
        [Remote(action: "CheckUserReceiverExistsByUniqueNumber", controller: "Transaction", ErrorMessage = "UserExistsErrorMessage")]
        [Display(Name = "UniqueNumberDisplay")]
        public string ReceiverUniqueNumber { get; set; }

        [Required(ErrorMessage = "TransferAmountErrorMessage")]
        [DataType(DataType.Currency)]
        [Remote(action: "CheckUserBalance", controller: "Transaction", ErrorMessage = "UserBalanceErrorMessage")]
        [Display(Name = "TransferAmountDisplay")]
        public int TransferAmount { get; set; }

        public int UserBalance { get; set; }
    }
}
