﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "UserUniqueNumberErrorMessage")]
        [Display(Name = "UniqueNumberDisplay")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "PasswordRequiredErrorMessage")]
        [DataType(DataType.Password)]
        [Display(Name = "PasswordDisplay")]
        public string Password { get; set; }

        [Display(Name = "RememberMeDisplay?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
