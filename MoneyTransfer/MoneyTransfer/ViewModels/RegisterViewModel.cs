﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "EmailRequiredErrorMessage")]
        [Display(Name = "EmailDisplay")]
        public string Email { get; set; }

        [Required(ErrorMessage = "PasswordRequiredErrorMessage")]
        [DataType(DataType.Password)]
        [Display(Name = "PasswordDisplay")]
        public string Password { get; set; }

        [Required(ErrorMessage = "PasswordConfirmRequiredErrorMessage")]
        [Compare("Password", ErrorMessage = "PasswordCompareErrorMessage")]
        [DataType(DataType.Password)]
        [Display(Name = "PasswordConfirmDisplay")]
        public string PasswordConfirm { get; set; }
    }
}
