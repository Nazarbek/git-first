﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Services
{
    public class TransactionService
    {
        private ApplicationContext context;

        public TransactionService(ApplicationContext context)
        {
            this.context = context;
        }

        public async void AddTransaction(Transaction transaction)
        {
            await context.Transactions.AddAsync(transaction);
            await context.SaveChangesAsync();
        }

        public IQueryable<Transaction> GetAllTransactions()
        {
            return context.Transactions.Include(tr => tr.UserSender).Include(tr => tr.UserReceiver);
        }
    }
}
