﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Services
{
    public class UserService
    {
        private ApplicationContext context;

        public UserService(ApplicationContext context)
        {
            this.context = context;
        }

        public UserService()
        {
        }

        public async Task<User> GetUserByUniqueNumber(string uniqueNumber)
        {
            User user = await context.Users.FirstOrDefaultAsync(u => u.UniqueNumber == uniqueNumber);
            return user;
        }
    }
}
