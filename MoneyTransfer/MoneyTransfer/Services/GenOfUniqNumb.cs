﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MoneyTransfer.Models;

namespace MoneyTransfer.Services
{
    public class GenOfUniqNumb
    {
        private readonly UserManager<User> _userManager;
        
        public GenOfUniqNumb(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public GenOfUniqNumb()
        {
        }

        public string GenerateSixDigitRandomNumber()
        {
            Random random = new Random();
            string generatedNumber = string.Empty;
            for (int i = 1; i < 7; i++)
            {
                int randomNext = random.Next(0, 10);
                generatedNumber = generatedNumber + randomNext.ToString();
            }
            return generatedNumber;
        }

        public bool IsUnique(string uniqueNumber)
        {
            IList<User> users = _userManager.Users.ToList();
            foreach (User user in users)
            {
                if (!user.UserName.Contains(uniqueNumber))
                {
                    return true;
                }
            }
            return false;
        }

        public string GenerateUserUniqueNumber()
        {
            bool isNumberUnique;
            string uniqueNumber;
            do
            {
                uniqueNumber = GenerateSixDigitRandomNumber();
                isNumberUnique = IsUnique(uniqueNumber);

            } while (!isNumberUnique);

            return uniqueNumber;
        }
    }
}
