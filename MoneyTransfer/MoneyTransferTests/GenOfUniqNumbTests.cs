﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyTransfer.Services;
using Xunit;

namespace MoneyTransfer.Tests
{
    public class GenOfUniqNumbTests
    {
        [Fact]
        public void RegisterUserUniqNumberCountEqualsSix()
        {
            GenOfUniqNumb generation = new GenOfUniqNumb();
            var result = generation.GenerateSixDigitRandomNumber();
            Assert.Equal(6, result.Length);
        }

    }
}
