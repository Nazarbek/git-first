﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Controllers;
using MoneyTransfer.Models;
using MoneyTransfer.Services;
using Moq;
using Xunit;

namespace MoneyTransfer.Tests
{
    public class AccountControllerTests
    {
        private UserManager<User> userManager;

        [Fact]
        public async void RegisterUserUniqNumberIsUnique()
        {
            var mock = new Mock<UserManager<User>>();
            GenOfUniqNumb generation = new GenOfUniqNumb(mock.Object);
            var uniqueNumber = generation.GenerateUserUniqueNumber();
            var resultUser = await userManager.FindByNameAsync(uniqueNumber);
            Assert.NotNull(resultUser);
        }
    }
}
